@extends('layouts.app')

@section('content')
    <h1>Edit Post</h1>
    {!! Form::open(['action' => ['PostsController@update', $post->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
            {{Form::label('title', 'Title')}}
            {{Form::text('title', $post->title, ['class' => 'form-control', 'placeholder' => 'Title'])}}
        </div>
        <div class="form-group">
            {{Form::label('prix', 'Prix')}}
            {{Form::text('prix', $post->prix, ['class' => 'form-control', 'placeholder' => '0'])}}
        </div>
        <div class="form-group">
            {{Form::label('desc', 'Desc')}}
            {{Form::textarea('desc', $post->desc, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Description '])}}
        </div>
        <div class="form-group">
            {{Form::label('image', 'Image URL')}}
            {{Form::text('image', $post->image, ['class' => 'form-control', 'placeholder' => ''])}}
        </div>
        {{Form::hidden('_method','PUT')}}
        {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection