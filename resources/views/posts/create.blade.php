@extends('layouts.app')

@section('content')
    <h1>Create post</h1>
    {!! Form::open(['action' => 'PostsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
            {{Form::label('title', 'Title')}}
            {{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Title'])}}
        </div>
        <div class="form-group">
            {{Form::label('prix', 'Prix')}}
            {{Form::text('prix', '', ['class' => 'form-control', 'placeholder' => '0'])}}
        </div>
        <div class="form-group">
            {{Form::label('desc', 'Desc')}}
            {{Form::textarea('desc', '', ['class' => 'form-control', 'placeholder' => 'Description'])}}
        </div>
        <div class="form-group">
            {{Form::label('image', 'Image URL')}}
            {{Form::text('image', '', ['class' => 'form-control', 'placeholder' => ''])}}
        </div>
        {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection