@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    You are logged in!
                    @if(count($posts) > 0)
                        <table class="table table-striped">
                            <tr>
                                <th>Title</th>
                                @if(Auth::user()->role == 'admin')<th></th>@endif
                                <th></th>
                                <th></th>
                            </tr>
                            @foreach($posts as $post)
                                <tr>
                                    <td>
                                        {{$post->title}}
                                        @if($post->isDraft == 1)
                                        <small>( Draft )</small>
                                        @endif
                                    </td>
                                    <td>
                                    @if(Auth::user()->role == 'admin')
                                        @if($post->isDraft == 1)
                                        {!!Form::open(['action' => ['PostsController@publishPost', $post->id], 'method' => 'POST','class' => 'pull-right'])!!}
                                            {{Form::hidden('_method', 'PUT')}}
                                            {{Form::submit('Publish', ['class' => 'btn btn-primary'])}}
                                        {!!Form::close()!!}
                                        @else
                                        {!!Form::open(['action' => ['PostsController@moveDraftPost', $post->id], 'method' => 'POST','class' => 'pull-right'])!!}
                                            {{Form::hidden('_method', 'PUT')}}
                                            {{Form::submit('Move to draft', ['class' => 'btn btn-warning'])}}
                                        {!!Form::close()!!}
                                        @endif
                                    @endif
                                    </td>
                                    <td><a href="posts/{{$post->id}}/edit" class="btn btn-default pull-right">Edit</a></td>
                                    <td>
                                        {!!Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                                            {{Form::hidden('_method', 'DELETE')}}
                                            {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
                                        {!!Form::close()!!}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        <p>You have no posts</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
