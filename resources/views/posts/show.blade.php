@extends('layouts.app')

@section('content')
    @if(!Auth::guest())
        @if(Auth::user()->role == 'admin' ||Auth::user()->id == $post->user_id)
        <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                    <a href="{{$post->id}}/edit" class="btn btn-primary">Edit</a>
                    </div>
                    <div class="col-sm-6">      
                    {!!Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST'])!!}
                        {{Form::hidden('_method', 'DELETE')}}
                        {{Form::submit('Delete', ['class' => 'btn btn-danger pull-right'])}}
                    {!!Form::close()!!}
                    </div>
                </div>
            </div>            
        @endif
    @endif
    <h1>{{$post->title}}</h1>
    <h3>Prix : {{$post->prix}}</h3>
    <img width="300px" height="300px" src="{{$post->image}}">
    <br><br>
    <div>
        <p>{{$post->desc}}</p>
    </div>
    <hr>
<small>{{$post->created_at}} par {{$post->user->name}} </small>
    <hr>
    <br>
    @if(count($post->comments) > 0)
        @foreach ($post->comments as $com)
            <div class="well">
                <h3>{{$com->user->id}}</h3>
                <small>{{$com->created_at}}</small>
                <p>{{$com->body}}</p>
            </div>
        @endforeach
    @else 
        <p>No post</p>
    @endif
    <br><br>
    {!! Form::open(['action' => ['CommentsController@store', $post->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
            {{Form::textarea('body', '', ['class' => 'form-control', 'placeholder' => 'Commentaire...'])}}
        </div>
        {{Form::submit('Comment', ['class'=>'btn btn-success pull-right'])}}
    {!! Form::close() !!}
    <br><br>
@endsection