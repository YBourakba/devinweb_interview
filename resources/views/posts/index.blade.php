@extends('layouts.app')

@section('content')
    <h1>Posts</h1>
    @if(!Auth::guest())
        <a class="btn btn-primary btn-lg" href="posts/create" role="button">Create</a>
    @endif
    @if(count($posts) > 0)
        @foreach ($posts as $post)
            <div class="well">
                <h3><a href="posts/{{$post->id}}">{{$post->title}}</a></h3>
            </div>
        @endforeach
        {{$posts->links()}}
    @else 
        <p>No post</p>
    @endif
@endsection